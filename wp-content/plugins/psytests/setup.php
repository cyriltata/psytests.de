<?php
/**
 * Page In Page plugin setup
 */

// Define required plugin constants
define('PSYTESTS_TEXT_DOMAIN', 'Psytests_TXT_Domain');
define('PSYTESTS_ROOT', dirname(__FILE__));
define('PSYTESTS_ASSETS', PSYTESTS_ROOT . '/assets');
define('PSYTESTS_CLASSPATH', PSYTESTS_ROOT . '/classes');
define('PSYTESTS_TEMPLATES', PSYTESTS_ROOT . '/templates');
define('PSYTESTS_FUNCTIONS', PSYTESTS_ROOT . '/functions');

// DB Version control
define('PSYTESTS_DB_VERSION', '0');

// Require necessary files
require PSYTESTS_FUNCTIONS . '/config.php';
require PSYTESTS_FUNCTIONS . '/functions.php';

require PSYTESTS_ROOT . '/lib/Request.php';

require PSYTESTS_CLASSPATH . '/loader.php';

// Initialize config
Psytests_Config::init();

date_default_timezone_set(Psytests_Config::get('timezone'));
