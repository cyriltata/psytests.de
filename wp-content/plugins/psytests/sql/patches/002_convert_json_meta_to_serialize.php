<?php

/* 
 * Convert json stored meta values to serialized onces
 * @global $wpdb
 */

/* @var $wpdb wpdb */

// A. Study Meta
$psytests_meta_data = $wpdb->get_results("select post_id, meta_key, meta_value from $wpdb->postmeta where meta_key='study_meta'", OBJECT);
psytests_update_json_pm_data($psytests_meta_data);

// B. Newsletter Schedule Meta
$psytests_meta_data = $wpdb->get_results("select post_id, meta_key, meta_value from $wpdb->postmeta where meta_key='newsletter_schedule'", OBJECT);
psytests_update_json_pm_data($psytests_meta_data);

// C. Newsletter Test Meta
$psytests_meta_data = $wpdb->get_results("select post_id, meta_key, meta_value from $wpdb->postmeta where meta_key='newsletter_test'", OBJECT);
psytests_update_json_pm_data($psytests_meta_data);
