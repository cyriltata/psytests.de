<?php

$queries = array(
	"
	CREATE TABLE newsletters (
	  id int(10) unsigned NOT NULL,
	  title text NOT NULL,
	  content longtext NOT NULL,
	  schedule int(10) unsigned NOT NULL,
	  triggered int(11) NOT NULL DEFAULT '0',
	  sender_name varchar(255) NOT NULL,
	  sender_email varchar(255) NOT NULL,
	  deliveries int(10) unsigned NOT NULL DEFAULT '0',
	  failures int(10) unsigned DEFAULT '0',
	  PRIMARY KEY (id)
	) ENGINE=MyISAM DEFAULT CHARSET=utf8;
	",

	"
	CREATE TABLE `newsletters_mailinglist` (
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `names` varchar(225) NOT NULL,
	  `email` varchar(225) NOT NULL,
	  PRIMARY KEY (`id`),
	  UNIQUE KEY `email` (`email`)
	) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
	"
);
