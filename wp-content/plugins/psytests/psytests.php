<?php

/**
 * Plugin Name: Psytests
 * Plugin URI: http://psytests.de
 * Description: Plugin for psytests.de specific functionality
 * Version: 1.0
 * Author: Cyril Tata
 * Author URI: http://cyriltata.blogspot.com
 * License: GPL2
 */

require_once dirname(__FILE__) . '/setup.php';

// Register activation hook
register_activation_hook(__FILE__, 'psytests_activate');

// Run plugin functionality
add_action('init', array('Psytests_Fe', 'run'));

// Run admin functionality
if (is_admin()) {
	add_action('init', array('Psytests_Admin', 'run'));
}