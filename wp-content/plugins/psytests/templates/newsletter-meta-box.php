<?php

// Add an nonce field so we can check for it later.
call_user_func_array('wp_nonce_field', $nonce);
$values = get_post_meta($post->ID, 'newsletter_schedule', true);
$time = isset($values['time']) ? (int)$values['time'] : 0;
$recipient_lists = Psytests_Config::get('mailists');
?>

<div class="study-metabox newsletter">
	<div class="form-group">
		<label class="form-label"><?php _e('Date'); ?> (Format: dd/mm/yyyy)</label>
		<input class="form-control" size="2" maxlength="2" max="31" min="1" type="number" placeholder="dd" name="day" value="<?php echo $time ? date('d', $time) : ''; ?>" /> /
		<input class="form-control" size="2" maxlength="2" max="12" min="1" type="number" placeholder="mm" name="month" value="<?php echo $time ? date('m', $time) : ''; ?>" /> /
		<input class="form-control" size="4" maxlength="4" max="2099" min="2014" type="number" placeholder="yyyy" name="year" value="<?php echo $time ? date('Y', $time) : ''; ?>" />
	</div>

	<div class="form-group">
		<label class="form-label"><?php _e('Time'); ?> (Format: hh:mm - 24h)</label>
		<input class="form-control" size="2" maxlength="2" max="23" min="0" type="number" placeholder="hh" name="hour" value="<?php echo $time ? date('H', $time) : ''; ?>" /> :
		<input class="form-control" size="2" maxlength="2" max="59" min="0" type="number" placeholder="mm" name="minute" value="<?php echo $time ? date('i', $time) : ''; ?>" />
	</div>
	
	<div class="form-group">
		<i><?php if ($time) echo date('r', $time); ?></i>
	</div>

	<div class="form-group">
		<label class="form-label"><?php _e('Sender Name'); ?></label>
		<input type="text" class="form-control" name="sender_name" value="<?php psytests_fvalue('sender_name', $values); ?>" />
	</div>

	<div class="form-group">
		<label class="form-label"><?php _e('Sender Email'); ?></label>
		<input type="text" class="form-control" name="sender_email" value="<?php psytests_fvalue('sender_email', $values); ?>" />
	</div>

	<div class="form-group">
		<label class="form-label"><?php _e('Recipeints List'); ?></label>
		<select name="maillist">
			<?php foreach ($recipient_lists as $list => $name): ?>
			<option value="<?php echo $list; ?>" <?php psytests_fvalue('maillist', $values, 'option', $list); ?>><?php echo $name; ?></option>
			<?php endforeach; ?>
		</select>
	</div>
	<hr />
	<div class="form-group">
		<h4>Note that if you re-schedule your newsletter to a future time, it will be sent again at that time even if it was already sent.</h4>
	</div>
</div>

