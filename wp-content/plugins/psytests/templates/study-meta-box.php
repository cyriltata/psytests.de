<?php

// Add an nonce field so we can check for it later.
call_user_func_array('wp_nonce_field', $nonce);
$values = (array) get_post_meta($post->ID, 'study_meta', true);
?>

<div class="study-metabox">
	<div class="form-group">
		<label class="form-label"><?php _e('Researcher'); ?></label>
		<input type="text" class="form-control" name="researcher" value="<?php psytests_fvalue('researcher', $values); ?>" />
	</div>
	
	<div class="form-group">
		<label class="form-label"><?php _e('Link'); ?></label>
		<input type="text" class="form-control" name="link" value="<?php psytests_fvalue('link', $values); ?>" />
	</div>
	
	<div class="form-group">
		<label class="form-label"><?php _e('External Link'); ?></label>
		<input type="text" class="form-control" name="external_link" value="<?php psytests_fvalue('external_link', $values); ?>" />
	</div>
	
	<div class="form-group">
		<label class="form-label"><?php _e('Time'); ?></label>
		<textarea class="form-control" name="time"><?php psytests_fvalue('time', $values); ?></textarea>
	</div>
	
	<div class="form-group">
		<label class="form-label"><?php _e('Participants'); ?></label>
		<input type="text" class="form-control" name="participants" value="<?php psytests_fvalue('participants', $values); ?>" />
	</div>
	
	<div class="form-group">
		<label class="form-label"><?php _e('Feedback'); ?></label>
		<textarea class="form-control" name="feedback" style="width: 400px;"><?php psytests_fvalue('feedback', $values); ?></textarea>
	</div>
	
	<div class="form-group">
		<label class="form-label"><?php _e('Status'); ?></label>
		<input type="radio" class="form-control" name="status" value="1" <?php psytests_fvalue('status', $values, 'radio', 1); ?> /><?php _e('Active'); ?> &nbsp;
		<input type="radio" class="form-control" name="status" value="0" <?php psytests_fvalue('status', $values, 'radio', 0); ?> /><?php _e('Not Active'); ?>
	</div>

	<hr />
	<div class="form-group">
		<label class="form-label"><?php _e('Show Newsletter Form'); ?></label>
		<input type="checkbox" class="form-control" name="show_newsletter" value="1" <?php psytests_fvalue('show_newsletter', $values, 'radio', 1); ?> /><?php _e(''); ?>
	</div>
	<div class="form-group">
		<label class="form-label"><?php _e('Alert to users'); ?></label>
		<textarea class="form-control" name="alert" style="width: 400px;"><?php psytests_fvalue('alert', $values); ?></textarea>
	</div>

</div>

