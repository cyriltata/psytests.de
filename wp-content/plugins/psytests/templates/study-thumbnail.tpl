<li class="{css_class}">
	
	<div class="item-thumbs center-bg" style="background-image: url('{image_url}'); height: {css_height}px; max-height: {css_height}px;">
		<a class="hover-wrap" title="{title}" href="{link}">
			<span class="overlay-img"></span>
			<span class="overlay-img-thumb font-icon-plus"></span>
		</a>
		<span class="study-state state-{status}"></span>
	</div>
	<h4 class="small-title fixed-text-height"><a href="{link}">{raw_title}</a></h4>
	<span class="text-center block-bottom"><a href="{link}" class="btn btn-default">Zur Studie</a></span>
</li>