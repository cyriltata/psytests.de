<h4 class="heading">{title}</h4>
<div class="row">
	<section id="projects">
		<ul id="thumbs" class="portfolio">
			{studies}
		</ul>
		<div class="clearfix"></div>
		<div class="col-md-12 text-center">
			{more_text}
		</div>
	</section>
</div>