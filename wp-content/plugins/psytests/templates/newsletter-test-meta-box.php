<?php

// Add an nonce field so we can check for it later.
$values = get_post_meta($post->ID, 'newsletter_test', true);
?>

<div class="study-metabox newsletter">
	<div class="form-group">
		<?php _e('Test on save'); ?> <input type="checkbox" class="form-control" name="test_active" value="1" <?php psytests_fvalue('test_active', $values, 'checkbox', 1); ?> /> <br />
		<input type="text" class="form-control" name="test_email" placeholder="Recipient E-mail" value="<?php psytests_fvalue('test_email', $values); ?>" />
	</div>
</div>

