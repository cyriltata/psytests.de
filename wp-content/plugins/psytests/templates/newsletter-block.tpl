<h4 class="heading">{title}</h4>
<div class="row">
	<section id="newsletter">
		<div class="col-md-12 text-center">
			{description}
			<form action="{link}" method="post" class="form-inline newsletter" role="form">
				<div class="form-group">
					<input type="hidden" name="action" value="psytests_newsletter" />
					<input type="text" class="form-control col-xs-12" placeholder="Name" name="names" />
					<input type="email" class="form-control col-xs-12" placeholder="E-mail" name="email" />
					<select class="form-control" name="do">
						<option value="ein" selected="selected">eintragen</option>
						<option value="aus">austragen</option>
					</select>
					&nbsp; <button type="button" class="btn btn-default form-control" onclick="window.psytests.newsletter(this);">SENDEN</button>
				</div>
				<div id="newsletter-message"></div>
			</form>
		</div>
	</section>
</div>