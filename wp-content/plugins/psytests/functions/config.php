<?php

function psytests_config() {
	
$config = array(
	'plugin_css' => array(plugins_url('psytests/assets/app.css')),
	'plugin_js' => array(plugins_url('psytests/assets/app.js')),
);

$config['timezone'] = 'Europe/Berlin';

// mailing list recipients as configured in newsletterman app
// The key should be the name of the mail list in newsletterman and the value should be a human understandable name
$config['mailists'] = array(
	'default' => 'Default Psytests Mailinglist',
	'tobias' => 'Tobias\'s List',
);

return $config; 


}