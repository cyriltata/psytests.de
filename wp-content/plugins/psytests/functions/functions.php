<?php

/**
 * Required simple plugin functions
 */
function psytests_install_database() {
	global $wpdb;

	/* @var $queries array */
	require_once PSYTESTS_ROOT . '/sql/schema.php';

	foreach ($queries as $query) {
		$wpdb->query($query);
	}

	Psytests_Config::add('db_version', '0', true);
}

function psytests_update_database() {
	global $wpdb;

	$db_version = (int) Psytests_Config::option('db_version');
	$files = psytests_db_patches();
	if (!$files) {
		return true;
	}

	$queries = array();
	/* @var $queries array */
	foreach ($files as $version => $file) {
		if ($version > $db_version) {
			require_once $file;
		}
	}

	foreach ($queries as $query) {
		$wpdb->query($query);
	}

	if (!empty($version)) {
		$db_version = $version;
	}

	Psytests_Config::add('db_version', $db_version, false);
	return true;
}

function psytests_activate() {
	// set default settings
	if (!Psytests_Config::option('installed')) {
		psytests_install_database();
		psytests_update_database();
		return Psytests_Config::add('installed', 1, true);
	} else {
		return psytests_update_database();
	}
}

// check if all necessary functions and classes are present
// @todo list other functions and classes to check
function psytests_is_runnable() {
	$functions = array();
	$classes = array('Psytests', 'Psytests_Admin', 'Psytests_Config', 'Psytests_Fe');

	foreach ($functions as $function) {
		if (!function_exists($function)) {
			throw new Exception("Psytests plugin: Required function '{$function}' not found.");
		}
	}

	foreach ($classes as $class) {
		if (!class_exists($class, false)) {
			throw new Exception("Psytests plugin: Required class '{$class}' not found.");
		}
	}

	$log_file = PSYTESTS_ROOT . '/errors.log';
	if (!file_exists($log_file) && !file_put_contents($log_file, date('r'))) {
		throw new Exception("Psytests plugin: Could not create error log file at $log_file");
	}

}

add_action('init', 'psytests_is_runnable');

function psytests_db_patches() {
	$files = array();
	$update_path = PSYTESTS_ROOT . '/sql/patches';
	if (!is_dir($update_path)) {
		return $files;
	}

	if (($handle = opendir($update_path))) {
		while (false !== ($file = readdir($handle))) {
			$number = substr($file, 0, strpos($file, '_'));
			if (in_array(substr($file, -4), array('.php')) && is_numeric($number)) {
				$files[(int) $number] = $update_path . '/' . $file;
			}
		}
		closedir($handle);
		ksort($files);
	}
	return $files;
}

// enqueue scripts
function psytests_enqueue_scripts() {
	$scripts = Psytests_Config::get('plugin_js');
	if ($scripts) {
		foreach ($scripts as $index => $script) {
			wp_enqueue_script('psytests-js-' . $index, $script, array('jquery'));
		}
	}

	wp_localize_script('psytests-js-' . $index, 'psytests', array(
		'ajax_url' => wp_nonce_url(admin_url('admin-ajax.php'), 'psytests_ajax', 'psytests_ajax_nonce'),
	));
	$styles = Psytests_Config::get('plugin_css');
	if ($styles) {
		foreach ($styles as $index => $style) {
			wp_enqueue_style('psytests-css-' . $index, $style);
		}
	}
}

add_action('wp_enqueue_scripts', 'psytests_enqueue_scripts');
add_action('admin_init', 'psytests_enqueue_scripts');

// log a string or exception. Gives uniformity to plugin's error logs
function psytests_log($e, $type = '') {
	$log_file = PSYTESTS_ROOT . '/errors.log';
	if ($e instanceof Exception) {
		error_log('PSYTESTS: ' . $type . $e->getMessage(). "\n", 3, $log_file);
		error_log('PSYTESTS: ' . $e->getTraceAsString(). "\n", 3, $log_file);
		return;
	}

	error_log('PSYTESTS: ' . print_r($e, true) . "\n", 3, $log_file);
}

// set a variable if key exists as an index of array
function psytests_fvalue($key, $array, $type = false, $type_value = false, $return = false) {
	if (empty($key) || empty($array))
		return;
	if (!isset($array[$key]))
		return;

	$value = $array[$key];
	if (in_array($type, array('radio', 'checkbox')) && $type_value == $value) {
		echo 'checked="checked"';
		return;
	}
	if ($type == 'option' && $type_value == $value) {
		echo 'selected="selected"';
		return;
	}
	if ($return === true) {
		return esc_attr($value);
	}
	echo esc_attr($value);
}

function psytests_maketime($date, $time = '') {
	$date = explode("/", $date);
	if (count($date) != 3 || !checkdate($date[1], $date[0], $date[2])) {
		return false;
	}

	if ($time) {
		$time = explode(":", $time);
		if (count($time) != 2 || $time[0] < 0 || $time[1] < 0 || $time[0] > 23 || $time[1] > 59) {
			return false;
		}
	} else {
		$time = array(0, 0);
	}

	return mktime($time[0], $time[1], 0, $date[1], $date[0], $date[2]);
}

function psytests_replace($string, array $params) {
	foreach ($params as $key => $value) {
		$string = str_replace('{' . $key . '}', $value, $string);
	}
	return $string;
}

function psytests_get_template($name) {
	$file = PSYTESTS_TEMPLATES . "/$name.tpl";
	if (!file_exists($file)) {
		throw new Exception("Psytests: Template $name.tpl not found");
	}
	return file_get_contents($file);
}

function psytests_email_content_type() {
	return 'text/html';
}

/**
 * 
 * @param array $psytests_meta_data
 */
function psytests_update_json_pm_data($psytests_meta_data) {
	if (empty($psytests_meta_data)) {
		return;
	}

	foreach ($psytests_meta_data as $data) {
		$data = (array) $data;
		$post_id = $data['post_id'];
		$meta_key = $data['meta_key'];
		$meta_value = (array) json_decode($data['meta_value']);
		update_post_meta($post_id, $meta_key, $meta_value);
	}
}

function psytests_dummy_image($width, $height) {
	return "http://dummyimage.com/{$width}x{$height}/efefef/fff&text=Psytests.de";
}
