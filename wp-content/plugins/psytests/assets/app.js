window.psytests = window.psytests || {};
(function($) {
	"use strict";
	
	function post(url, data, callback) {
		$.ajax({
			type: 'POST',
			url: url,
			data: data,
			dataType: 'json',
			success: function (response, code, jqxhr) {
				callback(response);
			},
			error: function (jqxhr, errText) {
				message('Ajax Error', 'There was an error in the ajax request ' + errText);
			},
			beforeSend: function (jqxhr) {
				//alert(this.url);
			}
		});
	}

	function spinner() {
		return ' <i class="fa fa-spinner fa-spin"></i>';
	}

	function formObjectVars($form) {
		var values = {};
		$.each($form.serializeArray(), function(i, o) {
			values[o.name] = o.value;
		});
		return values;
	}

	function removeSpinner($object) {
		$object.find('.fa-spinner').remove();
	}

	function hasSpinner($object) {
		return $object.find('.fa-spinner').length > 0;
	}

	function blinkActiveStudies() {
		$('.study-state.state-1').fadeOut(500);
		$('.study-state.state-1').fadeIn(500);
	}

	var app = {
		init: function() {
			if ($('.study-state').length) {
				setInterval(blinkActiveStudies, 1000);
			}
		},
		newsletter: function(button) {
			var $form = $(button).parents('form');
			var data = formObjectVars($form);
			var $msger = $form.find('#newsletter-message');

			if (hasSpinner($form)) {
				return;
			}

			$form.find('button').append(spinner());
			post(psytests.ajax_url, data, function(response) {
				$msger.removeClass().text('');
				if (!response || !response.success) {
					$msger.addClass('alert alert-danger').text(response.message);
				} else {
					$form.find('.form-control').val('');
					$msger.addClass('alert alert-success').text(response.message);
				}
				removeSpinner($form);
			});
		},
		contact: function(button) {
			var $form = $(button).parents('form');
			var data = formObjectVars($form);
			var $msger = $('#contact-message');

			if (hasSpinner($form)) {
				return;
			}

			$form.find('button').append(spinner());
			post(psytests.ajax_url, data, function(response) {
				$msger.removeClass().text('');
				if (!response || !response.success) {
					$msger.addClass('alert alert-danger').text(response.message);
				} else {
					$form.find('.form-control').val('');
					$msger.addClass('alert alert-success').text(response.message);
					$form.remove();
				}
				removeSpinner($form);
			});
		}
	};

	window.psytests = $.extend(window.psytests, app);
	$(document).ready(app.init);
})(jQuery);


