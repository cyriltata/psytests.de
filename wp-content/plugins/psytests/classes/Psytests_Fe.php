<?php

class Psytests_Fe extends Psytests {

	private static $instance = null;

	protected function __construct() {
		// register short codes
		parent::__construct();
	}

	/**
	 * @return Psytests_Fe
	 */
	public static function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public static function run() {
		parent::run();
		$me = self::getInstance();
		$me->addShortcodes();
		$me->registerPostTypes();
	}

	protected function addShortcodes() {
		add_shortcode('psytests_studies', array($this, 'shortcodeStudies'));
		add_shortcode('psytests_newsletter', array($this, 'shortcodeNewsletter'));
	}

	public function shortcodeStudies($attributes) {
		global $post;
		$attributes = shortcode_atts(array('title' => '', 'count' => 4, 'active' => 'no'), $attributes);
		$perpage = $attributes['count'] > 0 ? $attributes['count'] : -1;
		$limitcount = 0;
		$more_text = $post->post_name === 'home' ? '(Die Gesamtübersicht der Projekte erhalten Sie über den Menüpunkt <a href="/s-studien/">Studien</a>.)' : '<p></p>';

		if ($attributes['active'] === 'yes') {
			$limitcount = $perpage;
			$perpage = -1;
		}

		$query = new WP_Query(array(
			'post_type' => 'study',
			'posts_per_page' => $perpage,
		));

		$study_thumbnail = psytests_get_template('study-thumbnail');
		$study_block = psytests_get_template('study-block');
		$count = 0;
		$studies = array();

		if ($query->posts) :
			foreach ($query->posts as $post) {
		
				$meta = $values = get_post_meta($post->ID, 'study_meta', true);
				// check if we are getting only active studies for some particular count
				if ($attributes['active'] == 'yes' && ($limitcount <= $count || $meta['status'] != 1)) {
					continue;
				}

				$excerpt = apply_filters('get_the_excerpt', $post->post_excerpt);
				$overview = $excerpt ? $excerpt : wp_trim_excerpt($excerpt);
				$image = $image_url = psytests_dummy_image(600, 600);

				if (has_post_thumbnail($post->ID)) {
					list ($image_url, $image_width, $image_height) = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
				}

				$studies[] = array(
					'title' => htmlspecialchars(get_the_title($post), ENT_QUOTES|ENT_HTML401, 'UTF-8'),
					'raw_title' => get_the_title($post),
					'image_url' => $image_url,
					'image' => $image,
					'overview' => $overview,
					'link' => get_the_permalink($post),
					'status' => $meta['status'],
				);

				$count++;
			}
			
			if (($count = count($studies)) === 0) {
				return null;
			}

			$css_class = 'col-lg-3 col-md-3 col-sm-6 col-xs-12';
			$css_height = 195;
			if (($count % 3 === 0 && $count % 4 !== 0 && $count / 4 < 2) || $count === 5) {
				$css_class = 'col-lg-4 col-md-4 col-sm-12 col-xs-12';
				$css_height = 295;
			}

			$studies_string = '';
			foreach ($studies as $study) {
				$study['css_class'] = $css_class;
				$study['css_height'] = $css_height;
				$studies_string .= psytests_replace($study_thumbnail, $study);
			}

			$code = psytests_replace($study_block, array(
				'title' => $attributes['title'],
				'studies' => $studies_string,
				'more_text' => $more_text,
			));
		endif;

		wp_reset_query();
		return $code;
	}

	public function shortcodeNewsletter($attributes) {
		$attributes = shortcode_atts(array(
			'title' => '',
			'description' => 'Möchten Sie erfahren, wenn wir neue Onlinestudien anbieten? Der PSYTESTS-Newsletter wird maximal monatlich versandt und informiert Sie über alle Neuigkeiten auf PSYTESTS. 
			Wenn Sie ihn erhalten möchten, geben Sie bitte Ihre E-Mailadresse in das folgende Feld ein und wählen Sie die Option "eintragen".
			Sollten Sie Ihre E-Mailadresse zu einem späteren Zeitpunkt aus unserer Datenbank löschen wollen, können Sie das hier ebenfalls jederzeit tun.'
		), $attributes);

		$newsletter_block = psytests_get_template('newsletter-block');

		return psytests_replace($newsletter_block, array(
			'title' => $attributes['title'],
			'description' => $attributes['description'],
			'link' => '#',
		));
	}
	

}
