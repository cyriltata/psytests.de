<?php

class Psytests_Admin extends Psytests {
	
	private static $instance = null;

	protected function __construct() {
		parent::__construct();
	}

	/**
	 * 
	 * @return Psytests_Admin
	 */
	public static function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public static function run() {
		parent::run();
		$me = self::getInstance();
		$me->registerPostTypes();
		add_action('add_meta_boxes', array($me, 'addMetaBoxes'));
		add_action('save_post_study', array($me, 'saveStudy'));
		add_action('save_post_newsletter', array($me, 'saveNewsletter'));
		add_action('wp_ajax_nopriv_psytests_newsletter', array($me, 'subscribeNewsletter'));
		add_action('wp_ajax_psytests_newsletter', array($me, 'subscribeNewsletter'));
		add_action('wp_ajax_nopriv_psytests_contact', array($me, 'contactSubmit'));
		add_action('wp_ajax_psytests_contact', array($me, 'contactSubmit'));
	}

	public function addMetaBoxes() {
		add_meta_box('psytest_studies_mb', 'Other Details', array($this, 'renderView'), 'study', 'normal', 'high', array('view' => 'study-meta-box'));
		add_meta_box('psytest_newsletter_mb', 'Schedule', array($this, 'renderView'), 'newsletter', 'normal', 'high', array('view' => 'newsletter-meta-box'));
		add_meta_box('psytest_newsletter_test_mb', 'Test Newsletter', array($this, 'renderView'), 'newsletter', 'side', 'default', array('view' => 'newsletter-test-meta-box'));
	}

	public function saveStudy($post_id) {
		$params = $this->request->getParams();

		// We need to verify this came from the our screen and with proper authorization,
		// because save_post can be triggered at other times.

		// Check if our nonce is set.
		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if (!$this->validateNonce() || (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)) {
			return $post_id;
		}

		// Check the user's permissions.
		if (!current_user_can('edit_post', $post_id)) {
			return $post_id;
		}

		// OK to save input.
		$meta = array(
			'researcher' => $params['researcher'],
			'link' => $params['link'],
			'external_link' => $params['external_link'],
			'time' => $params['time'],
			'participants' => $params['participants'],
			'feedback' => $params['feedback'],
			'status' => (int)$params['status'],
			'show_newsletter' => (int)$params['show_newsletter'],
			'alert' => $params['alert'],
		);

		// Update the meta field.
		update_post_meta($post_id, 'study_meta', $meta);
		return $post_id;
	}

	public function saveNewsletter($post_id) {
		$params = $this->request->getParams();

		// We need to verify this came from the our screen and with proper authorization,
		// because save_post can be triggered at other times.

		// Check if our nonce is set.
		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if (!$this->validateNonce() || (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)) {
			return $post_id;
		}

		// Check the user's permissions.
		if (!current_user_can('edit_post', $post_id)) {
			return $post_id;
		}

		$params['date'] = implode('/', array($params['day'], $params['month'], $params['year']));
		$params['time'] = implode(':', array($params['hour'], $params['minute']));
		if (!($time = psytests_maketime($params['date'], $params['time']))) {
			return false;
		}

		$post = get_post($post_id);
		$newsletter = array(
			'id' => $post_id,
			'title' => html_entity_decode(apply_filters('the_title', $post->post_title)),
			'content' => apply_filters('the_content', $post->post_content),
			'schedule' => $time,
			'sender_name' => $params['sender_name'],
			'sender_email' => is_email($params['sender_email']) ? $params['sender_email'] : '',
			'maillist' => $params['maillist'],
		);

		$meta = array(
			'time' => $time,
			'sender_name' => $params['sender_name'],
			'sender_email' => $params['sender_email'],
			'maillist' => $params['maillist'],
		);

		$meta_test = array(
			'test_active' => (int) !empty($params['test_active']),
			'test_email' => is_email($params['test_email']) ? $params['test_email'] : '',
		);

		// Update the meta field.
		update_post_meta($post_id, 'newsletter_schedule', $meta);
		update_post_meta($post_id, 'newsletter_test', $meta_test);

		// If not publishing newsletter, do not schedule it for sending
		if (get_post_status($post_id) !== 'publish') {
			$newsletter['schedule'] = 0;
		}

		// If schedule is greater than now force trigger to 0
		if ($newsletter['schedule'] > time()) {
			$newsletter['triggered'] = 0;
		} elseif ($newsletter['schedule'] > 0) {
			$newsletter['triggered'] = time();
		}

		// Save in newsletters table
		$exists = $this->db->get_var("SELECT id FROM newsletters WHERE id = $post_id LIMIT 1");
		if ($exists) {
			$this->db->update('newsletters', $newsletter, array('id' => $post_id), array('%d', '%s', '%s', '%d', '%s', '%s', '%s'), array('%d'));
		} else {
			$this->db->insert('newsletters', $newsletter, array('%d', '%s', '%s', '%d', '%s', '%s', '%s'));
		}

		// Send newsletter test
		if ($meta_test['test_active']) {
			$this->testNewsletter($post_id, $meta_test['test_email'], $newsletter['sender_email'], $newsletter['sender_name']);
		}

		return $post_id;
	}

	private function testNewsletter($id, $recipient_email, $sender_email, $sender_name = null) {
		if (!$id || !is_email($recipient_email) || !is_email($sender_email)) {
			return false;
		}

		$newsletter = $this->db->get_row("SELECT id, title, content FROM newsletters WHERE id = $id", OBJECT);
		if (!$newsletter) {
			return false;
		}

		if (!$sender_name) {
			$sender_name = $sender_email;
		}

		add_filter('wp_mail_content_type', 'psytests_email_content_type');
		$sent = wp_mail($recipient_email, $newsletter->title, $newsletter->content, "From: $sender_name <$sender_email>" . "\r\n");
		remove_filter('wp_mail_content_type', 'psytests_email_content_type');
		return $sent;
	}

}