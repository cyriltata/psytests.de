<?php

class Psytests {

	protected $errors = array();
	protected $successes = array();
	protected $template_path;

	const PLUGIN = 'psytests/psytests.php';

	protected $nonce = array();

	/**
	 *
	 * @var wpdb
	 */
	protected $db;

	/**
	 *
	 * @var Request
	 */
	protected $request;

	protected function __construct() {
		global $wpdb;

		$this->template_path = PSYTESTS_TEMPLATES;
		$this->request = new Request();
		$this->nonce = array('psytests_nonce', md5(PSYTESTS_ROOT));
		$this->db = $wpdb;
	}

	protected function addError($s) {
		$this->errors[] = $s;
	}

	protected function addSuccess($s) {
		$this->successes[] = $s;
	}

	protected function hasErrors() {
		return !empty($this->errors);
	}

	protected function hasSuccess() {
		return !empty($this->successes);
	}

	protected function getErrors() {
		return $this->errors;
	}

	protected function getSuccesses() {
		return $this->successes;
	}

	protected function resetMessges() {
		$this->errors = array();
		$this->successes = array();
	}

	protected function validateNonce() {
		$key = $this->nonce[0];
		$field = $this->nonce[1];
		$nonce = $this->request->str($field);
		if (!$nonce) {
			return false;
		}
		return wp_verify_nonce($nonce, $key);
	}

	protected function registerPostTypes() {
		// Type Study
		register_post_type('study', array(
			'labels' => array(
				'name' => __('Studies'),
				'singular_name' => __('Study')
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'studien'),
			'query_var' => true,
			'menu_icon' => 'dashicons-format-status',
			'supports' => array(
				'title',
				'excerpt',
				'thumbnail',
				'page-attributes',
				'template',
			)
		));
		// Type News Letter
		register_post_type('newsletter', array(
			'labels' => array(
				'name' => __('Newsletters'),
				'singular_name' => __('Newsletter')
			),
			'public' => true,
			'has_archive' => false,
			'query_var' => true,
			'menu_icon' => 'dashicons-pressthis',
			'supports' => array(
				'title',
				'excerpt',
				'editor',
			)
		));
		flush_rewrite_rules();
	}

	public static function run() {
		// l18n translation files
		$locale = get_locale();
		$dir = trailingslashit(PSYTESTS_ROOT . '/lang/');
		$mofile = "{$dir}{$locale}.mo";
		load_textdomain(PSYTESTS_TEXT_DOMAIN, $mofile);

		// plugin links
		$plugin = self::PLUGIN;
		add_filter("plugin_action_links_$plugin", array(__CLASS__, 'pluginLinks'));
	}

	public static function pluginLinks($links) {
		$links[] = '<a href="#">' . __('Studies', PSYTESTS_TEXT_DOMAIN) . '</a>';
		$links[] = '<a href="#">' . __('Newsletters', PSYTESTS_TEXT_DOMAIN) . '</a>';
		return $links;
	}

	public function renderView($view, $vars = array()) {
		if ($view instanceof WP_Post) {
			// If we are here then we came in from a metabox
			$newVars = array();
			$newVars['post'] = $view;

			$view = $vars['args']['view'];
			$vars = $newVars;
		}
		$vars['nonce'] = $this->nonce;
		extract($vars);
		include $this->template_path . "/{$view}.php";
	}

	public function subscribeNewsletter() {
		if (!$this->request->isXMLHttpRequest() || !$this->request->isPostRequest()) {
			die();
		}

		$email = $this->request->str('email');
		$nonce = $this->request->str('psytests_ajax_nonce');
		$do = $this->request->str('do');

		if (!is_email($email) || !wp_verify_nonce($nonce, 'psytests_ajax') || !in_array($do, array('ein', 'aus'))) {
			$this->jsonResponse(array(
				'success' => false,
				'message' =>  __('Sie haben entweder eine ungültige Emailadresse oder Einstellung angegeben.'),
			));
		}

		$exists = $this->db->get_var($this->db->prepare('SELECT count(*) from newsletters_mailinglist WHERE email = %s', $email));
		$success = true;

		if ($do === 'ein' && !$exists) {
			$this->db->insert('newsletters_mailinglist', array(
				'names' => $this->request->str('names'),
				'email' => $email,
				'created' => time(),
			));
			$message = __('Danke, dass Sie den Newsletter abonniert haben!');
		} elseif ($do === 'aus' && $exists) {
			$this->db->delete('newsletters_mailinglist', array('email' => $email));
			$message = __('Danke erfolgreich vom Newsletter abgemeldet haben');
		} else {
			$success = false;
			$message = $do === 'ein' ? __('Diese E-Mail-Adresse ist bereits in unserer Datenbank registriert') : __('Diese E-Mail existiert nicht');
		}

		$this->jsonResponse(array(
			'success' => $success,
			'message' => $message,
		));
	}

	public function contactSubmit() {
		if (!$this->request->isXMLHttpRequest() || !$this->request->isPostRequest()) {
			die();
		}

		$name = $this->request->str('name');
		$email = $this->request->str('email');
		$subject = $this->request->str('subject');
		$comments = $this->request->str('comments');
		$nonce = $this->request->str('psytests_ajax_nonce');
		$emailTo = iwebtheme_smof_data('contact_email');

		if (!is_email($email) || !wp_verify_nonce($nonce, 'psytests_ajax')) {
			$this->jsonResponse(array(
				'success' => false,
				'message' =>  __('You either entered an invalid email or sent an invalid request'),
			));
		}

		if (!$name || !$comments) {
			$this->jsonResponse(array(
				'success' => false,
				'message' =>  __('Please fill in all required fields. A name and a message'),
			));
		}

		$body = "Name: $name \n\nEmail: $email \n\nMessage: $comments";
		$headers = 'From: Psytests.de <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;

		if (wp_mail($emailTo, $subject, nl2br($body), $headers)) {
			$this->jsonResponse(array(
				'success' => true,
				'message' =>  __('Thank You! Your message has been sent'),
			));
		} else {
			$this->jsonResponse(array(
				'success' => false,
				'message' =>  __('An unexpected error occured and your email could not be sent'),
			));
		}
		
	}

	public function jsonResponse(array $data) {
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header('Content-Type: application/json');
		echo json_encode($data);
		exit(0);
	}

}
