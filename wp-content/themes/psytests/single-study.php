<?php
/**
 * @package WordPress
 * @subpackage Moderna Theme
 */
$sidebar_pos = iwebtheme_smof_data('sidebar_pos');
?>
<?php get_header(); ?>
<section id="inner-headline">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<?php get_template_part('includes/breadcrumbs'); ?>
				</div>
			</div>	
		</div>
</section>		
<section id="content">
<div class="container">
	<div class="row">
	<?php if ($sidebar_pos == 'left') { ?>
		<?php get_sidebar(); ?>
	<?php } ?>	
	<div class="col-lg-8">

				<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php 
						$values = get_post_meta($post->ID, 'study_meta', true);
						$study_link = (!empty($values['status']) && !empty($values['link'])) ? $values['link'] : '';
					?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="post-heading">
							<h3><a href="<?php echo $study_link; ?>"><?php the_title(); ?></a></h3>
						</div>
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
							<div class="post-image col-md-4" style="padding-left: 0px;">
								<a href="<?php echo $study_link; ?>"><?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?></a>
							</div>
							<div class="entry-summary col-md-8">
								<?php the_excerpt(); ?>
								<?php if (!empty($values['status']) && empty($values['alert'])): ?>
									<div class="block text-left" style="margin-top: 12px;">
										<a href="<?php echo $study_link; ?>" class="btn btn-default btn-large btn-study"><?php _e('Zur Studie'); ?></a>
									</div>
								<?php endif; ?>
							</div>
						<?php else: ?>
							<div class="entry-summary col-md-12">
								<?php the_excerpt(); ?>
							</div>
						<?php endif; ?>
						<div class="clearfix"></div>
						
						<?php if (!empty($values['alert'])): ?>
							<div class="col-md-12"><strong><?php echo $values['alert']; ?></strong></div>
						<?php endif; ?>
						<div class="clearfix"></div>
						<?php if (!empty($values['show_newsletter'])): ?>
							<div class="col-md-12 small-newsletter">
								<?php echo do_shortcode('[psytests_newsletter title="", description=""]'); ?>
							</div>
						<?php endif; ?>
						<div class="clearfix"></div>
						
						<div class="post-meta study" style="overflow:hidden;">
							<div class="block">
								<span class="col-md-3 col-xs-12"><i class="fa fa-user"></i> <b><?php _e('Studienleiter'); ?></b></span>
								<span class="col-md-9 col-xs-12"><?php psytests_fvalue('researcher', $values); ?></span>
								<div class="clearfix"></div>
							</div>
							<div class="block">
								<span class="col-md-3 col-xs-12"><i class="fa fa-link"></i> <b><?php _e('Link'); ?></b></span>
								<span class="col-md-9 col-xs-12"><a href="<?php psytests_fvalue('link', $values); ?>" style="color:blue;"><?php psytests_fvalue('link', $values); ?></a></span>
								<div class="clearfix"></div>
							</div>
							<div class="block">
								<span class="col-md-3 col-xs-12"><i class="fa fa-clock-o"></i> <b><?php _e('Dauer'); ?></b></span>
								<span class="col-md-9 col-xs-12"><?php echo nl2br(psytests_fvalue('time', $values, false, false, true)); ?></span>
								<div class="clearfix"></div>
							</div>
							<div class="block">
								<span class="col-md-3 col-xs-12"><i class="fa fa-users"></i> <b><?php _e('Zielgruppe'); ?></b></span>
								<span class="col-md-9 col-xs-12"><?php psytests_fvalue('participants', $values); ?></span>
								<div class="clearfix"></div>
							</div>
							<div class="block">
								<span class="col-md-3 col-xs-12"><i class="fa fa-table"></i> <b><?php _e('Rückmeldung'); ?></b></span>
								<span class="col-md-9 col-xs-12"><?php psytests_fvalue('feedback', $values); ?></span>
								<div class="clearfix"></div>
							</div>
							<div class="block">
								<span class="col-md-3 col-xs-6"><i class="fa fa-check-circle"></i> <b><?php _e('Status'); ?></b></span>
								<span class="col-md-9 col-xs-6">
									<?php if (!empty($values['status'])): ?>
									<span class="label label-success"><?php _e('Aktiv'); ?></span>
									<?php else: ?>
									<span class="label label-danger"><?php _e('Nicht Aktiv'); ?></span>
									<?php endif; ?>
								</span>
								<div class="clearfix"></div>
							</div>
							<?php if (!empty($values['status']) && empty($values['alert'])): ?>
							<div class="block text-center" style="margin-top: 50px;">
								<a href="<?php echo $study_link; ?>" class="btn btn-default btn-large btn-study"><?php _e('Zur Studie'); ?></a>
							</div>
							<?php endif; ?>
							
							<div class="clearfix"></div>
						</div>
					
					</article><!-- #post -->
				<?php endwhile; ?>
				
				<?php 
					$previous = get_previous_post_link('%link');
					$next = get_next_post_link('%link');
				?>
				<?php if ($previous) { ?>
					<span class="pull-left"><i class="fa fa-arrow-left"></i> <?php echo $previous; ?></span>
				<?php } ?>
				<?php if ($next) { ?>
					<span class="pull-right"><?php echo $next; ?> <i class="fa fa-arrow-right"></i></span>
				<?php } ?>
					
				<?php else : ?>
					<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; ?>
	</div>	
	<?php if ($sidebar_pos == 'right') { ?>
		<?php //get_sidebar(); ?>
	<?php } ?>
	</div>
</div>    
</section>	
<?php get_footer(); ?>