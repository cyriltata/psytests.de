<?php
/*
  Template Name: Contact page
 */
get_header();
?>
<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<?php get_template_part('includes/breadcrumbs'); ?>
			</div>
		</div>	
	</div>
</section>	
<section id="content" class="nopadtop">
	

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h4 style="font-weight: normal; margin: 40px 0px 0px 0px;">Georg-Elias-Müller-Institut für Psychologie</h4>
				Gosslerstraße 14<br />
				37073 Göttingen<br /><br />
				<a href="mailto:psytests@uni-goettingen.de"><i class="fa fa-envelope"></i> psytests@uni-goettingen.de</a><br />
				<a href="//www.psytests.de"><i class="fa fa-globe"></i> www.psytests.de</a>
			</div>

		</div>
		
		<?php if (iwebtheme_smof_data('map_enable') != 0) { ?>
			<div id="googlemaps" class="google-map">
			</div>		
		<?php } ?>
	</div>
</section>

<?php get_footer(); ?>