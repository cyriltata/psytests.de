# Makefile [Psytests.de]

INSTALL_DIR=$(PSYTEST_ROOT)/var/www/psytests

GIT_REPO=https://cyriltata@bitbucket.org/cyriltata/psytests.de.git
GIT=$(shell echo "git --git-dir=$(INSTALL_DIR)/.git --work-tree=$(INSTALL_DIR)")
APACHE_USER=www-data
APACHE_GROUP=www-data
APACHE_CONF=/etc/apache/sites-available/psytests.conf
APACHE_CONF_SSL=/etc/apache/sites-available/psytests-ssl.conf

COMPOSER=composer

all: install

install: install_files clean
	@echo "psytests.de has been installed successfully to $(INSTALL_DIR)."
	@echo "Go apache settings in $(APACHE_CONF) and $(APACHE_CONF_SSL)";
	@echo "And finally open the site on browser and complete installation";

install_files:
	@echo "Installing files .....";

	@install -d -m 0744 $(INSTALL_DIR)

	$(GIT) init
	$(GIT) remote add origin $(GIT_REPO)
	$(GIT) pull origin master
	cd $(INSTALL_DIR) && git submodule update --init --recursive

	cp $(INSTALL_DIR)/wp-config-sample.php $(INSTALL_DIR)/wp-config.php
	@chown -R $(APACHE_USER):$(APACHE_GROUP) $(INSTALL_DIR)

	@echo "Done."

uninstall:
	@echo "Un-installing ....";
	@rm -rf $(INSTALL_DIR)
	@echo "Done."

update: update_files clean
	@echo "Updating.... Done"

update_files:
	$(GIT) reset --hard
	$(GIT) pull origin master

clean:
	@echo "Installation completed..."