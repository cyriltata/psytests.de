# Psytests.de (Creating a git repo for wordpress site)

## Installing locally for inital development
---------

### Set up project directory

- mkdir psytests && cd psytests
- git init
- touch README.md
- git add README.md
- git commit -m "Initial Commit"

### Add wordpress from git repo as a submodule into a 'core' directory

- git submodule add https://github.com/Wordpress/Wordpress.git core
- git commit -m  "Add Wordpress Core"

### Checkout the current stable version of wordpress and go back to root directory
- cd core
- git checkout 4.0.1
- cd ..
- git commit -am "Checkout WordPress 4.0.1"


### We don't want to make changes to the core wordpress so we will have to move things abit

#### Moving config file.

- cp core/wp-config-sample.php .
- git add wp-config-sample.php
- git commit -m "Adding default wp-config file"

Note that in production you will have to rename this file to 'wp-config.php'. **DO NOT COMMIT ACTUAL CONFIG TO REPO**

Now we can enter **default** configuration for wordpress and commit changes to our main repository

#### Moving themes and plugins into our project repo and remove uncessary themes and content

Keep whatever theme you want to use as default theme

- cp -R core/wp-content .
- rm wp-content/plugins/hello.php
- rm -rf wp-content/themes/twentythirteen
- rm -rf wp-content/themes/twentytwelve
- git add wp-content
- git commit -m "Add default wordpress directory"

### Copy index.php file well all requests are routed

- cp core wordpress/index.php .
- git add index.php
- git commit -m "Adding index.php"

### Configure WordPress to let it know how to talk to everything

#### Edit index.php

Change the line `require ('./wp-blog-header.php')` to `require ('.core//wp-blog-header.php')`

- git commit -am "Pointing index.php to correct location"

### Edit wp-config-sample.php

Add the following constant definitions to your wordpress site

*Site & Home URLs will change since wordpress core installation has been moved to 'core' folder*

`define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/core');`

`define('WP_HOME', 'http://' . $_SERVER['SERVER_NAME']);`

*wp-content dir*

`define('WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/wp-content');`

`define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/wp_content');`

*Default theme*

`define('WP_DEFAULT_THEME', 'twentyfourteen')`

** save changes **

- git commit -am "Setting default configuration"


### TODO: Setup Develop branch for gitflow

## Install necessary external plugins

When website is up and running then you can install and activate additional external plugins. These should be kept independent of default installation.
Only site's specific themes and plugins should be added to repository in the newly created 	`wp-content` directory

At this point you can do the usual wordpress setting things like importing sample data, settting up menus, ... etc

## Comming changes local changes while taking care of git submodules

See [Git Submodules](http://git-scm.com/book/en/v2/Git-Tools-Submodules)


# Updating WordPress Core
-------------------------

To update the wordpress core, go into the core subdirectory (in our case *core*) and fetch tags and specify which tags to update

- cd core
- git fetch --tags
- git checkout x.x.x
- cd ..
- git commit -m "Update WordPress to version x.x.x"

